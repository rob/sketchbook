Table table;
String p = ".";
String q = "-";
Integer i;
Integer x;
Integer y;
Integer u = 0;

TableRow vocabrow;
TableRow qrow;

PFont liberation;

//liberation = loadFont("Noto Serif Tibetan ExtraLight");
//Noto Serif Tibetan ExtraLight

void setup() {
  liberation = createFont("LiberationMono-Bold.ttf", 30);
  table = loadTable("m1.csv", "header");
  //fullScreen();
  size(1920,1080);
  frameRate(30);
  smooth();
  background(0);
  //textAlign(LEFT);
}

Integer itteration = 0;
Integer linepace = 4;
Integer pace = 4;
Integer wordpace = 4;

void draw() {
  y = frameCount % 4; // elements in questions
  x = frameCount % 30; // in vocab
  i = frameCount; //% (60*30); // 60 seconds @ 30fps
   
  if (i <= 1800) { itteration = 0;}
  else if (i <= 2000) { itteration = 1;}
  else if (i <= 2500) { itteration = 2;}
  else if (i <= 3000) { itteration = 3;}
  else if (i <= 3500) { itteration = 4;}
  else if (i <= 4000) { itteration = 5;}

  else {itteration = 0;}
  
  itteration = itteration % 6; // 5 items in sequence

// pace increments / tempo
   pace = i % 400; // 9 seconds
   wordpace = i % 12;
/////////////////////////////////////////////////////////

  if ( itteration == 0 ){  // text question decoration lines   
    //background(0);
    qrow = table.getRow(i%29);
    q = qrow.getString("vocab");
    fill(100,200); 
    textSize(random(12,44));
    text(q,random(width) - 100, random(height) + random(55)); 
    //text(q,random(width) - 100, 50 + random(55)); 
    //u = u % 3;

  } else {};
  
  if ( pace == 10 && itteration == 0 ){ 
    background(0);
    qrow = table.getRow(u);
    q = qrow.getString("questions");
    fill(255); 
    textSize(100);
    text(q,width/6, height/6, 1200,850);
    //fill(255,14);               // white opacity
    //rect(0,0, width, height);   
    u = u+1 % 3;
} 
/////////////////////////////////////////////////////////

  else if (
  wordpace == 0 && itteration == 1 ) {
    //background(155);
    vocabrow = table.getRow(i%29);
    p = vocabrow.getString("vocab"); 
    fill(random(111, 255 )); 
    textSize(random(33,111));
    text(p,random(width/2), random(height)); 
    fill(0,33);               // white opacity
    rect(0,0, width, height);
  } 
  
/////////////////////////////////////////////////////////  

  else if (
  //wordpace == 0 && 
  itteration == 2 ) {
    //background(155);
    vocabrow = table.getRow(i%29);
    p = vocabrow.getString("vocab"); 
    fill(random(255)); 
    textSize(random(33,77));
    text(p,random(width/2), random(height)); 
    //fill(0,55);               // white opacity
    //rect(0,0, width, height);
  }
  
    else if (
  //wordpace == 0 && 
  itteration == 3 ) {
    vocabrow = table.getRow(i%29);
    p = vocabrow.getString("vocab"); 
    fill(random(44, 255)); 
    textSize(random(24,88));
    text(p,random(width/2), random(height)); 
    
 fill(255);
 if (i%20 == 19){
    fill(255);
  } 
   fill(0);

  if (i%20 == 10) {fill(0);} 
    
    textSize(600);
    text("NAGIB", 0, height/1.5);
  }

//////////////////////////////////////////

      else if (
  wordpace == 0 && 
  itteration == 4 ) {
    //background(0);

    vocabrow = table.getRow(i%29);
    p = vocabrow.getString("vocab"); 
    fill(random(155, 255)); 
    textSize(random(20,50));
    text(p,random(width)-100, random(height)); 
    fill(0,12);               // white opacity
    rect(0,0, width, height);

 if (i%20 == 19){
    fill(255);
  } 
   fill(255);

  if (i%20 == 10) {fill(255);} 
    textSize(250);
    text(" b r e z \n O d r a ", 0, height/3);
  }
//////////////////////////////////////////

      else if (
    wordpace == 0 && 
    itteration == 4 ) {
    //background(0);
    vocabrow = table.getRow(x%5);
    p = vocabrow.getString("brand"); 
    fill(random(155, 255)); 
    textSize(12);
    text(" blaj blja lkj sdfkj sdkj skj dj\n info@zavodrizoma.si",width/2, height/2); 

    }
  
  
  
  saveFrame("frames/####.tif");
  
}
